# Shopping Basket

This is a console app that uses a fluent builder to apply discounts to items in a shopping basket. 

## Discounts
- Buy one get one free
- Discount of 10% off the price
- Both discounts

## Usage

Run the application and press

- `b` to run **buy one get one free** 
- `d` to run **discount of 10% off the price**
- `a` to run **both discounts**
- `q` to quit the application

### Console Output Example

```
a
ITEM - Price: 10, Name: IWatch, QTY: 1
ITEM - Price: 9.99, Name: Ladies Watch, QTY: 3
ITEM - Price: 1.75, Name: Ladies Replacement Strap, QTY: 1
UPDATED QTY & PRICE - Price: 9, Name: IWatch, QTY: 2
UPDATED QTY & PRICE - Price: 8.99, Name: Ladies Watch, QTY: 4
UPDATED QTY & PRICE - Price: 1.58, Name: Ladies Replacement Strap, QTY: 2

b
ITEM - Price: 10, Name: IWatch, QTY: 1
ITEM - Price: 9.99, Name: Ladies Watch, QTY: 3
ITEM - Price: 1.75, Name: Ladies Replacement Strap, QTY: 1
UPDATED QTY - Price: 10, Name: IWatch, QTY: 2
UPDATED QTY - Price: 9.99, Name: Ladies Watch, QTY: 4
UPDATED QTY - Price: 1.75, Name: Ladies Replacement Strap, QTY: 2

d
ITEM - Price: 10, Name: IWatch, QTY: 1
ITEM - Price: 9.99, Name: Ladies Watch, QTY: 3
ITEM - Price: 1.75, Name: Ladies Replacement Strap, QTY: 1
UPDATED PRICE - Price: 9, Name: IWatch, QTY: 1
UPDATED PRICE - Price: 8.99, Name: Ladies Watch, QTY: 3
UPDATED PRICE - Price: 1.58, Name: Ladies Replacement Strap, QTY: 1
```