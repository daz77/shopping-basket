﻿using ShoppingBasket.DiscountHelper;
using System;
using Xunit;

namespace ShoppingBasketTests
{
    public class DiscountBuilderTests
    {
        [Fact]
        public void WithDiscount_ThrowsArgumentOutOfRangeExceptionWhenPercentageIsNotValid()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => new DiscountBuilder().WithDiscount(-1));
            Assert.Throws<ArgumentOutOfRangeException>(() => new DiscountBuilder().WithDiscount(101));
            Assert.Throws<ArgumentOutOfRangeException>(() => new DiscountBuilder().WithDiscount(0));
        }
    }
}
