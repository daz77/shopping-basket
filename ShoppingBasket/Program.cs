﻿using ShoppingBasket.DiscountHelper;
using System;

namespace ShoppingBasket
{
    class Program
    {
        static void Main(string[] args)
        {
            var items = BasketStore.ShoppingItems;

            bool bContinue = true;

            while(bContinue)
            {
                var key = Console.ReadKey();

                if(key.KeyChar == 'b')
                {
                    Console.WriteLine();

                    foreach (var item in items)
                    {
                        Console.WriteLine($"ITEM - Price: {item.Price}, Name: {item.Name}, QTY: {item.Quantity}");
                    }

                    try
                    {
                        var updatedItems = new DiscountBuilder()
                                            .WithBuyOneGetOneFree()
                                            .Create(items);

                        foreach (var item in updatedItems)
                        {
                            Console.WriteLine($"UPDATED QTY - Price: {item.Price}, Name: {item.Name}, QTY: {item.Quantity}");
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }

                    Console.WriteLine();
                }
                else if(key.KeyChar == 'd')
                {
                    Console.WriteLine();

                    foreach (var item in items)
                    {
                        Console.WriteLine($"ITEM - Price: {item.Price}, Name: {item.Name}, QTY: {item.Quantity}");
                    }

                    try
                    {
                        var updatedItems = new DiscountBuilder()
                                            .WithDiscount(10)
                                            .Create(items);

                        foreach (var item in updatedItems)
                        {
                            Console.WriteLine($"UPDATED PRICE - Price: {item.Price}, Name: {item.Name}, QTY: {item.Quantity}");
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }

                    Console.WriteLine();
                }
                else if(key.KeyChar == 'a')
                {
                    Console.WriteLine();

                    foreach (var item in items)
                    {
                        Console.WriteLine($"ITEM - Price: {item.Price}, Name: {item.Name}, QTY: {item.Quantity}");
                    }

                    try
                    {
                        var updatedItems = new DiscountBuilder()
                                            .WithDiscount(10)
                                            .WithBuyOneGetOneFree()
                                            .Create(items);

                        foreach (var item in updatedItems)
                        {
                            Console.WriteLine($"UPDATED QTY & PRICE - Price: {item.Price}, Name: {item.Name}, QTY: {item.Quantity}");
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }

                    Console.WriteLine();
                }
                else if(key.KeyChar == 'q')
                {
                    bContinue = false;
                }
            }
        }
    }
}
