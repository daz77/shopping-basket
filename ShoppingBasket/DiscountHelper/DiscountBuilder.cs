﻿using ShoppingBasket.Models;
using System;
using System.Collections.Generic;

namespace ShoppingBasket.DiscountHelper
{
    public class DiscountBuilder : IDiscountBuilder
    {

        #region Private Fields
        private Action<ShoppingItem> buyOneGetOneFree;
        private Action<ShoppingItem> withDiscount;
        #endregion

        #region Public Fluent Methods
        public List<ShoppingItem> Create(List<ShoppingItem> shoppingItems)
        {
            List<ShoppingItem> result = new List<ShoppingItem>();

            foreach(ShoppingItem item in shoppingItems)
            {
                ShoppingItem updateItem = new ShoppingItem
                {
                    Id = item.Id,
                    Price = item.Price,
                    Name = item.Name,
                    Quantity = item.Quantity
                };

                buyOneGetOneFree?.Invoke(updateItem);
                withDiscount?.Invoke(updateItem);

                result.Add(updateItem);
            }

            return result;
        }
        
        public IDiscountBuilder WithBuyOneGetOneFree()
        {
            buyOneGetOneFree = item =>
            {
                item.Quantity = item.Quantity + 1;
            };

            return this;
        }

        public IDiscountBuilder WithDiscount(int percentage)
        {
            if(percentage < 1 || percentage > 100)
            {
                throw new ArgumentOutOfRangeException($"{percentage} is invalid");
            }

            withDiscount = item =>
            {
                item.Price = GetDiscountedPrice(percentage, item.Price);
            };

            return this;
        }
        #endregion

        #region Private Methods
        private double GetDiscountedPrice(int percentage, double price)
        {
            return Math.Round(price - ((price / 100D) * percentage), 2, MidpointRounding.AwayFromZero);
        }
        #endregion
    }
}
