﻿using ShoppingBasket.Models;
using System.Collections.Generic;

namespace ShoppingBasket.DiscountHelper
{
    public interface IDiscountBuilder
    {
        public IDiscountBuilder WithBuyOneGetOneFree();
        public IDiscountBuilder WithDiscount(int percentage);
        public List<ShoppingItem> Create(List<ShoppingItem> shoppingItems);
    }
}
