﻿using ShoppingBasket.Models;
using System.Collections.Generic;


namespace ShoppingBasket
{
    public static class BasketStore
    {
        #region Properties
        public static List<ShoppingItem> ShoppingItems { get; private set; }
        #endregion

        #region Constructor
        static BasketStore()
        {
            ShoppingItems = new List<ShoppingItem>()
            {
                new ShoppingItem {
                    Id = 1,
                    Price = 10.0,
                    Name = "IWatch",
                    Quantity = 1
                },
                new ShoppingItem {
                    Id = 5,
                    Price = 9.99,
                    Name = "Ladies Watch",
                    Quantity = 3
                },
                new ShoppingItem {
                    Id = 7,
                    Price = 1.75,
                    Name = "Ladies Replacement Strap",
                    Quantity = 1
                }
            };
        }
        #endregion
    }
}
